var FluidModel = require('./fluid/FluidModel'),
    FluidView = require('./fluid/FluidView'),
    FluidController = require('./fluid/FluidController'),
    StatsModel = require('./stats/StatsModel'),
    StatsView = require('./stats/StatsView');

/**
 *
 * @param {FluidModel} dataModel
 * @returns {StatsView}
 */
function initStats(dataModel) {
    var stats = new StatsModel(),
        view = new StatsView(document.querySelector('.stats'), stats);

    function updateStats() {
        stats.setTotal(dataModel.toJson().length);
        view.render(stats);
    }

    function onItemRemoved() {
        stats.reportDeleted();
        updateStats();
    }

    dataModel.on('insert', updateStats);
    dataModel.on('remove', onItemRemoved);
    return view;
}
/**
 * @type FluidModel
 */
var fluidModel = new FluidModel();
initStats(fluidModel);

new FluidController(fluidModel, new FluidView(document.querySelector('.container2')));
