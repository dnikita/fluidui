var EventEmitter = require('../common/EventEmitter');
/**
 * @class
 * @extends EventEmitter
 */
function StatModel() {
    this.total = 0;
    this.deleted = 0;
}

StatModel.prototype = new EventEmitter();
StatModel.prototype.constructor = StatModel;
/**
 * @type {number}
 */
StatModel.prototype.total = null;
/**
 * @type {number}
 */
StatModel.prototype.deleted = null;
/**
 *
 * @param {number} v
 */
StatModel.prototype.setTotal = function(v) {
    this.total = v;
    this.trigger('update');
};
/**
 *
 */
StatModel.prototype.reportDeleted = function() {
    this.deleted += 1;
    this.trigger('update');
};
/**
 *
 * @returns {{total: number, deleted: number}}
 */
StatModel.prototype.toJson = function() {
    return {
        total: this.total,
        deleted: this.deleted
    };
};

module.exports = StatModel;