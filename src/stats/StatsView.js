var View = require('../common/View');

/**
 *
 * @constructor
 * @param {HTMLElement} root
 * @param {StatsModel} model
 * @extends View
 */
function StatsView(root, model) {
    View.apply(this, arguments);
}

StatsView.prototype = new View();
/**
 *
 * @type {string}
 */
StatsView.prototype.template = [
    '<span>Boxes count:&nbsp;{{total}}</span>',
    '<span>Boxes removed:&nbsp;{{deleted}}</span>'
].join('');

module.exports = StatsView;