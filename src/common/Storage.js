var EventEmitter = require('./EventEmitter');

/**
 * @class
 * @extends EventEmitter
 * @constructor
 */
function Storage() {
    this._onStorageEvent = this._onStorageEvent.bind(this);
    window.addEventListener('storage', this._onStorageEvent);
}

Storage.prototype = new EventEmitter();

Storage.prototype._onStorageEvent = function() {
    this.trigger('update');
};

/**
 *
 * @param {string} k key
 * @param {string} v value
 */
Storage.prototype.save = function(k, v) {
    localStorage.setItem(k, v);
};
/**
 *
 * @param {string} k key
 */
Storage.prototype.load = function(k) {
    return localStorage.getItem(k);
};

module.exports = Storage;