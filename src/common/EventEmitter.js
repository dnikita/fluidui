/**
 * @class
 */
function EventEmitter() {
    this.listeners = {};
}
/**
 * subscribes a callback for the event
 * @param {string} name
 * @param {function} clb
 */
EventEmitter.prototype.on = function (name, clb) {
    if (typeof this.listeners[name] === 'undefined') {
        this.listeners[name] = [];
    }
    this.listeners[name].push(clb);
};
/**
 * notifies all the subscribers of the event, passing the data from the <code>name</code> param
 * @param {string} name
 * @param {*} [data] data to be passed to subscribers
 */
EventEmitter.prototype.trigger = function (name, data) {
    if (this.listeners[name]) {
        this.listeners[name].forEach(function (clb) {
            clb(data);
        })
    }
};
/**
 * provided a name of an event and a callback, unsubscribes it from the event
 * provided an event name, removes all the subscriptions for the event
 * provided no arguments, removes all the subscriptions from the emitter
 * @param {string} [name] event name
 * @param {function} [clb] callback
 */
EventEmitter.prototype.off = function(name, clb) {
    if (name) {
        if (clb) {
            if (this.listeners[name]) {
                this.listeners[name] = this.listeners[name] .filter(function(c) {
                    return c !== clb;
                });
            }
        } else {
            this.listeners[name] = [];
        }
    } else {
        this.listeners = {};
    }
};

module.exports = EventEmitter;