/**
 * @class
 * @param root
 */
function View(root) {
    this.root = root;
}
/**
 *
 * @type {string}
 */
View.prototype.template = '';
/**
 *
 * @param {Object} model
 * @returns {View}
 */
View.prototype.render = function(model) {
    this.root.innerHTML = this._fill(this.template, model.toJson());
    return this;
};
/**
 *
 * @param {string} template
 * @param {Object} obj key-value map for resolving {{var}} vars
 * @returns {string}
 * @protected
 */
View.prototype._fill = function (template, obj) {
    return template.replace(/{{(.+?)}}/g, function (m, k) {
        return typeof obj[k] !== 'undefined' ? obj[k] : '';
    });
};

module.exports = View;