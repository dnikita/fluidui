var View = require('../common/View');

/**
 * @class
 * @extends View
 * @param {HTMLElement} root
 */
function FluidView(root) {
    View.apply(this, arguments);
}

FluidView.prototype = new View();
/**
 * @type {string}
 */
FluidView.prototype.template = [
    '<div data-name="{{name}}" class="box {{color}}">',
        '<div class="row header">',
            '<div class="cell">{{name}}</div>',
            '<div class="cell">x</div>',
        '</div>',
        '<div class="row">',
            '<div class="cell">{{prev}}</div>',
            '<div class="cell">{{next}}</div>',
        '</div>',
    '</div>'
].join('');
/**
 *
 * @param {number} i index
 * @returns {string} color
 * @protected
 */
FluidView.prototype._getColorByIndex = function(i) {
    var color = '';
    switch (i % 4) {
        case 0:
            color = '';
            break;
        case 1:
            color = 'red';
            break;
        case 2:
            color = 'green';
            break;
        case 3:
            color = 'blue';
            break;
    }
    return color;
};
/**
 *
 * @param {FluidModel} model
 */
FluidView.prototype.render = function(model) {
    var html = '<div class="row">';
    var colCount = 0;
    var colPerRow = 3;

    for (var i = 0, l = model.boxes.length; i < l; i += 1) {
        html += this._fill(this.template, {
            name: model.boxes[i],
            color: this._getColorByIndex(i),
            prev: colCount ? model.boxes[i - 1] : '&nbsp',
            next: colCount < colPerRow - 1 ? model.boxes[i + 1] : '&nbsp'
        });

        colCount += 1;
        if (colCount === colPerRow) {
            html += '</div><div class="row">';
            colCount = 0;
            colPerRow -= 1;
            if (colPerRow === 0) {
                colPerRow = 3;
            }
        }
    }
    html += '</div>';
    this.root.innerHTML = html;
};

module.exports = FluidView;