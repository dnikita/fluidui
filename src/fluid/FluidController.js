var Storage = require('../common/Storage');

var container1 = document.querySelector('.container1'),
    container2 = document.querySelector('.container2'),
    notificationsRoot = document.querySelector('#notifications');

/**
 * @const
 * @type {string}
 */
var STORAGE_KEY = 'state',
    MOUSE_BUTTON = {
        LEFT: 0,
        RIGHT: 2
    };

/**
 * @class
 * @param {FluidModel} model
 * @param {FluidView} view
 */
function FluidController(model, view) {
    this.model = model;
    this.view = view;
    this.storage = new Storage();

    this._onMouseDown = this._onMouseDown.bind(this);
    this.view.root.addEventListener('mousedown', this._onMouseDown, true);
    this.view.root.addEventListener('contextmenu', this._onContextMenu, true);
    this.view.root.addEventListener('mouseleave', this._onMouseLeave);
    this.view.root.addEventListener('mouseover', this._onMouseOver);

    this._onBoxRemoved = this._onBoxRemoved.bind(this);
    this._onBoxInserted = this._onBoxInserted.bind(this);
    this.model.on('remove', this._onBoxRemoved);
    this.model.on('insert', this._onBoxInserted);
    this._loadFromStorage = this._loadFromStorage.bind(this);
    this.storage.on('update', this._loadFromStorage);
    this._loadFromStorage();
}
/**
 * @type {string} keeps track of what has been loaded from localStorage
 * introduced to avoid bug from IE9-10 misfiring 'storage' event
 */
FluidController.prototype.currentLoad = null;
/**
 *
 * @private
 */
FluidController.prototype._loadFromStorage = function() {
    var text = this.storage.load(STORAGE_KEY),
        data;

    try {
        data = JSON.parse(text)
    } catch (e) {
        if (console) {
            console.warn(e);
        }
    }
    if ((data instanceof Array) && this.currentLoad !== text) {
        this.model.setData(data);
        this.currentLoad = text;
    } else {
        this.model.setData([1]);
    }
};
/**
 * @param {Event} e
 * @protected
 */
FluidController.prototype._onContextMenu = function (e) {
    e.stopPropagation();
    e.preventDefault();
};
/**
 * @param {Event} e
 * @protected
 */
FluidController.prototype._onMouseDown = function (e) {
    var box = e.target;
    while (!box.getAttribute('data-name') && box.tagName !== 'BODY') {
        box = box.parentNode;
    }
    var id = box.getAttribute('data-name');
    if (id) {
        switch (e.button) {
            case MOUSE_BUTTON.LEFT:
                this.model.insertAfter(id);
                break;
            case MOUSE_BUTTON.RIGHT:
                this.model.remove(id);
                break;
        }
    }
    return false;
};
/**
 * @protected
 */
FluidController.prototype._onMouseLeave = function () {
    container1.style.border = 'solid 10px rgba(0,0,0,0)';
    container2.style.border = 'solid 15px rgba(0,0,0,0)';
};
/**
 * @protected
 */
FluidController.prototype._onMouseOver = function (e) {
    var box = e.target;
    while (!box.getAttribute('data-name') && box.tagName !== 'BODY') {
        box = box.parentNode;
    }
    if (box.getAttribute('data-name')) {
        container1.style.border = 'solid 10px black';
        container2.style.border = 'solid 15px black';
    }
};
/**
 * @protected
 */
FluidController.prototype._onModelUpdated = function () {
    this.storage.save(STORAGE_KEY, JSON.stringify(this.model.toJson()));
    this.view.render(this.model);
};
/**
 * @protected
 */
FluidController.prototype._onBoxRemoved = function (id) {
    this._onModelUpdated();
    this._showDeleteNotification(id);
};
/**
 * @protected
 */
FluidController.prototype._onBoxInserted = function () {
    this._onModelUpdated();
};
/**
 * @param {string} id an id of the deleted item
 * @protected
 */
FluidController.prototype._showDeleteNotification = function (id) {
    var notification = document.createElement('div');
    notification.textContent = 'deleted record# ' + id;
    notification.className = 'notification';
    notificationsRoot.appendChild(notification);
    setTimeout(function () {
        notification.className = 'notification hidden';
        setTimeout(function () {
            notification.parentNode.removeChild(notification);
        }, 3000);
    }, 0);
};

module.exports = FluidController;