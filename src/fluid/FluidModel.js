var EventEmitter = require('../common/EventEmitter');

/**
 * @class
 * @extends EventEmitter
 * @description Model for fluid layout boxes
 */
function FluidModel() {
    this.listeners = [];
    this.boxes = [];
}

FluidModel.prototype = new EventEmitter();
FluidModel.prototype.constructor = FluidModel;
/**
 * @param {string} id
 * @returns {number|null} index of element in the array if found
 * @protected
 */
FluidModel.prototype._getIndexById = function(id) {
    var index = this.boxes.indexOf(+id);
    return index !== -1 ? index : null;
};
/**
 * sets new data for the model
 * @param {number[]} boxes
 */
FluidModel.prototype.setData = function(boxes) {
    if (boxes instanceof Array) {
        this.boxes = boxes;
        this.trigger('insert');
    }
};
/**
 * inserts a new box after the given one
 * @param {string} id
 */
FluidModel.prototype.insertAfter = function(id) {
    var index = this._getIndexById(id);
    if (index !== null) {
        var newId = this.nextId();
        this.boxes.splice(index + 1, 0, newId);
        this.trigger('insert', newId);
    }
};
/**
 * removes the given box
 * @param {string} id
 */
FluidModel.prototype.remove = function(id) {
    var index = this._getIndexById(id);
    if (index !== null && this.boxes.length > 1) {
        this.boxes.splice(index, 1);
        this.trigger('remove', id);
    }
};
/**
 *
 * @returns {number} a minimal positive integer not present in the model
 */
FluidModel.prototype.nextId = function() {
    var aux = [];
    var result = 1;
    this.boxes.forEach(function (el) {
        aux[el] = 1;
    });
    while (typeof aux[result] !== 'undefined' && aux.length) {
        result += 1;
    }
    return result;
};
/**
 *
 * @returns {number[]}
 */
FluidModel.prototype.toJson = function() {
    return this.boxes.slice(0);
};

module.exports = FluidModel;