var gulp = require('gulp');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var connect = require('gulp-connect');
var nightwatch = require('gulp-nightwatch');
var jsdoc = require("gulp-jsdoc");
var jasmine = require('gulp-jasmine');

gulp.task('unittest', function () {
    return gulp.src('tests/unit/**/*.js')
        .pipe(jasmine());
});

gulp.task('doc', function() {
    gulp.src("./src/**/*.js")
        .pipe(jsdoc('./docs'));
});

gulp.task('nightwatch:chrome', function () {
    return gulp.src('')
        .pipe(nightwatch({
            configFile: './nightwatch.json',
            cliArgs: {
                env: 'chrome',
                tag: 'boxes'
            }
        }));
});

gulp.task('test', function () {
    connect.server({
        root: ['./static'],
        port: 3333
    });

    gulp.run('nightwatch:chrome', function () {
        connect.serverClose();
    });
});

gulp.task('serve', function() {
    connect.server({
        root: ['./static'],
        port: 8080
    });
});

function build(w) {
    return function() {
        var bundler = watchify(browserify('./src/app.js', {debug: true}));
        function rebundle() {
            bundler.bundle()
                .on('error', function(err) { console.error(err); this.emit('end'); })
                .pipe(source('bundle.js'))
                .pipe(buffer())
                .pipe(sourcemaps.init({ loadMaps: true }))
                .pipe(sourcemaps.write('./'))
                .pipe(gulp.dest('./static'));
        }
        if (w) {
            bundler.on('update', rebundle);
        }
        rebundle();
    }
}

gulp.task('watch', build(true));
gulp.task('build', build());
gulp.task('default', ['serve']);