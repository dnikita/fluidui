var names = require('./names.js');

module.exports = {
    '@tags': ['boxes', 'stats'],

    'basic stats': function (client) {
        client.url('http://localhost:3333')
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .moveToElement(names.cell2, 1, 1)
            .mouseButtonClick('right')
            .moveToElement(names.cell2, 1, 1)
            .mouseButtonClick('right')
            .moveToElement(names.cell2, 1, 1)
            .mouseButtonClick('right')
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .assert.containsText(names.statsRemoved, 'Boxes removed: 3')
            .assert.containsText(names.statsTotal, 'Boxes count: 5')
            .end();
    }
};