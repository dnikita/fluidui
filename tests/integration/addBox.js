var names = require('./names.js');

module.exports = {
    '@tags': ['boxes'],
    'beforeEach': function (client) {
        client.url('http://localhost:3333')
            .waitForElementVisible(names.root, 1000)
            .execute(function () {
                return document.querySelectorAll('.box');
            }, [], function (b) {
                client.assert.ok(b.value.length === 1, 'initially there is a single box');
            })
    },
    'simple adding': function (client) {
        client
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            // [1,5,4,3,2]
            .execute(function () {
                return document.querySelectorAll('.box');
            }, [], function (b) {
                client.assert.ok(b.value.length === 5, 'five boxes are present');
            })
            .end();
    },
    'adding location': function (client) {
        client
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell3)
            .click(names.cell1)
            // [1,5,3,2,4]
            .assert.attributeEquals(names.cell1, "data-name", "1")
            .assert.attributeEquals(names.cell2, "data-name", "5")
            .assert.attributeEquals(names.cell3, "data-name", "3")
            .assert.attributeEquals(names.cell4, "data-name", "2")
            .assert.attributeEquals(names.cell5, "data-name", "4")
            .end();
    }
};