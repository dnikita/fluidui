var names = require('./names.js');

module.exports = {
    '@tags': ['boxes'],
    'beforeEach': function (client) {
        client.url('http://localhost:3333')
            .waitForElementVisible(names.root, 1000)
            .execute(function () {
                return document.querySelectorAll('.box');
            }, [], function (b) {
                client.assert.ok(b.value.length === 1, 'initially there is a single box');
            })
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1);
        // [1,5,4,3,2]
    },
    'simple removal': function (client) {
        client
            .moveToElement(names.cell1, 1, 1)
            .mouseButtonClick('right')
            .moveToElement(names.cell3, 1, 1)
            .mouseButtonClick('right')
            .execute(function () {
                return document.querySelectorAll('.box');
            }, [], function (b) {
                client.assert.ok(b.value.length === 3, '3 boxes stay');
            })
            .assert.attributeEquals(names.cell1, "data-name", "5")
            .assert.attributeEquals(names.cell2, "data-name", "4")
            .assert.attributeEquals(names.cell3, "data-name", "2")
            .end();
    }
};