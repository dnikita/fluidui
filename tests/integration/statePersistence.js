var names = require('./names.js');

module.exports = {
    '@tags': ['boxes', 'persistence'],

    'persistence - save': function (client) {
        client.url('http://localhost:3333')
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .moveToElement(names.cell3, 1, 1)
            .mouseButtonClick('right')
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .execute(function () {
                return localStorage.getItem('state');
            }, [], function (b) {
                client.assert.ok(b.value === '[1,6,5,3,4,2]', 'state is persisted');
            })
            .end();
    },
    'persistence - load': function (client) {
        client.url('http://localhost:3333')
            .execute(function () {
                return localStorage.setItem('state', '[7,8,9,1,6,5,3,4,2]');
            }, [])
            .url('http://localhost:3333')
            .assert.attributeEquals(names.cell1, "data-name", "7")
            .assert.attributeEquals(names.cell2, "data-name", "8")
            .assert.attributeEquals(names.cell3, "data-name", "9")
            .assert.attributeEquals(names.cell4, "data-name", "1")
            .assert.attributeEquals(names.cell5, "data-name", "6")
            .assert.attributeEquals(names.cell6, "data-name", "5")
            .end();
    }
};