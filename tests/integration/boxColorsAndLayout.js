var names = require('./names.js');

module.exports = {
    '@tags': ['boxes'],
    'beforeEach': function (client) {
        client.url('http://localhost:3333')
            .waitForElementVisible(names.root, 1000)
            .execute(function () {
                return document.querySelectorAll('.box');
            }, [], function (b) {
                client.assert.ok(b.value.length === 1, 'initially there is a single box');
            })
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
            .click(names.cell1)
    },
    'colors order and layout': function (client) {
        client
            .assert.cssProperty(names.cell2, "backgroundColor", "rgba(255, 0, 0, 1)")
            .assert.cssProperty(names.cell3, "backgroundColor", "rgba(0, 128, 0, 1)")
            .assert.cssProperty(names.cell4, "backgroundColor", "rgba(0, 0, 255, 1)")
            .assert.cssProperty(names.cell6, "backgroundColor", "rgba(255, 0, 0, 1)")
            .assert.cssProperty(names.cell7, "backgroundColor", "rgba(0, 128, 0, 1)")
            .assert.cssProperty(names.cell8, "backgroundColor", "rgba(0, 0, 255, 1)")
            .end();
    }
};