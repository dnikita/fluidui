module.exports = {
    'root': '.container2',
    'statsTotal': '.stats > span:nth-of-type(1)',
    'statsRemoved': '.stats > span:nth-of-type(2)',
    'cell1': '.container2 > .row:nth-child(1) .box:nth-child(1)',
    'cell2': '.container2 > .row:nth-child(1) .box:nth-child(2)',
    'cell3': '.container2 > .row:nth-child(1) .box:nth-child(3)',
    'cell4': '.container2 > .row:nth-child(2) .box:nth-child(1)',
    'cell5': '.container2 > .row:nth-child(2) .box:nth-child(2)',
    'cell6': '.container2 > .row:nth-child(3) .box:nth-child(1)',
    'cell7': '.container2 > .row:nth-child(4) .box:nth-child(1)',
    'cell8': '.container2 > .row:nth-child(4) .box:nth-child(2)'
};