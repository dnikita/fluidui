var FluidModel = require('../../src/fluid/FluidModel');

describe("a fluid model", function() {
    it("triggers 'insert' on setData", function() {
        var model = new FluidModel();
        var clb = jasmine.createSpy();
        model.on('insert', clb);
        model.setData([1,2,3]);
        expect(clb).toHaveBeenCalled();
    });
    it("triggers 'insert' on insertAfter", function() {
        var model = new FluidModel();
        var clb = jasmine.createSpy();
        model.setData([1,2,3]);
        model.on('insert', clb);
        model.insertAfter('1');
        expect(clb).toHaveBeenCalled();
    });
    it("inserts a minimal positive number as a next id", function() {
        var model = new FluidModel();
        var clb = jasmine.createSpy();
        model.setData([1,3]);
        model.on('insert', clb);
        model.insertAfter('1');
        expect(clb).toHaveBeenCalledWith(2);
    });
    it("finds a minimal positive number as a next id", function() {
        var model = new FluidModel();
        model.boxes = [-1, 10, 5, 14, 1, 2 ];
        expect(model.nextId()).toEqual(3);
    });
    it("inserts an element right after the one clicked on", function() {
        var model = new FluidModel();
        model.setData([1,3]);
        model.insertAfter('1');
        model.insertAfter('1');
        expect(model.toJson()).toEqual([1,4,2,3]);
    });
    it("triggers 'remove' on remove", function() {
        var model = new FluidModel();
        var clb = jasmine.createSpy();
        model.setData([1,3]);
        model.on('remove', clb);
        model.remove('3');
        expect(clb).toHaveBeenCalledWith('3');
    });
    it("removes a clicked boxed", function() {
        var model = new FluidModel();
        model.setData([1,4,5,6,3]);
        model.remove('5');
        expect(model.toJson()).toEqual([1,4,6,3]);
        model.remove('4');
        expect(model.toJson()).toEqual([1,6,3]);
    });
});