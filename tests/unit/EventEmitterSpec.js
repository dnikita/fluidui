var EventEmitter = require('../../src/common/EventEmitter');

describe("an EventEmitter", function() {
    it("allows subscribing to events passing a callback", function() {
        var ee = new EventEmitter();
        var clb = jasmine.createSpy();
        var clb2 = jasmine.createSpy();
        ee.on('event', clb);
        ee.on('event2', clb2);
        ee.trigger('event');
        expect(clb).toHaveBeenCalled();
        expect(clb2).not.toHaveBeenCalled();
    });
    it("allows passing data when triggering an event", function() {
        var ee = new EventEmitter();
        var clb = jasmine.createSpy();
        ee.on('event', clb);
        ee.trigger('event', 123);
        expect(clb).toHaveBeenCalledWith(123);
    });
    it("allows unsubscribing", function() {
        var ee = new EventEmitter();
        var clb = jasmine.createSpy();
        ee.on('event', clb);
        ee.trigger('event');
        ee.trigger('event');
        ee.off('event', clb);
        ee.trigger('event');
        expect(clb.calls.count()).toEqual(2);
    });
});