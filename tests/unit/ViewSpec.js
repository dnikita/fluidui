var View = require('../../src/common/View');

describe("a base View", function() {
    it("can fill a template with custom values for variables", function() {
        var view = new View();
        var template = '{{var1}}a{{var1}}b{{var2}}';

        var res = view._fill(template,  {
            var1 : 1,
            var2 : 2
        });
        expect(res).toEqual('1a1b2');
    });
});