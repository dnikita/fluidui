# Fluid Layout with Boxes
## Running
Node.js and npm are required

``` 
npm install
gulp serve
```
the app will run on http://localhost:8080
## Code
There are no runtime dependencies for the app.

The JavaScript code is split into CommonJS modules: ./src
It is bundled with browserify.

Static resources are located in ./static
## Tests
Unit testing is done with Jasmine (http://jasmine.github.io/)

Specs location: ./tests/unit
```
gulp unittest
```
Integration testing is done with Nightwatch (http://nightwatchjs.org/)

Testcases location: ./tests/integration
```
gulp test
```
## Documentation
The code is documented with JSDoc.

Documentation can be generated with the following command:
```
gulp doc
```

## Environments
In general, should be IE9+ compliant
Tested with: 
 - **Desktop:** Win7 IE11, Win10 Edge, Win7\10 latest FF and Chrome, 
 - **Mobile:** Xperia Z1 Android 5, iPhone 5 iOS9\*

\* longtap does not delete boxes (which can be solved with additional code: handling of touch events)

## Conventions and limitations:
- left mouse click on a box creates a new box to the right of the clicked one (or in the beginning of the next line)
- right mouse click removes the clicked box
- on Android left and right mouse clicks maps to tap and long tap respectively
- state is saved on each change: box inserting\removal and is updated on all browser tabs where the app is open, in statistics, *'Boxes removed'* refers to the number of boxes removed in the current tab